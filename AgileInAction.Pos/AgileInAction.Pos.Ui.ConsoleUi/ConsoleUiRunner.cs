﻿using AgileInAction.Pos.Core;
using System;
using System.Collections.Generic;

namespace AgileInAction.Pos.Ui.ConsoleUi
{
    public class ConsoleUiRunner
    {
        private PointOfSale pos;

        public ConsoleUiRunner(PointOfSale pos)
        {
            this.pos = pos;
        }

        public void RunReadEvalPrintLoop()
        {
            var actions = new Dictionary<string, Action> {
                {"exit", () => Environment.Exit(0)},
                {"start-sale", pos.OnStartSale},
                {"show-total", pos.OnShowTotal},
                {"complete-sale", pos.OnCompleteSale}
            };

            while (true)
            {
                Console.Write("pos$ ");
                var input = Console.ReadLine();
                if (String.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine("You didn't enter anything");
                    continue;
                }

                Action action;
                actions.TryGetValue(input, out action);
                try
                {
                    if (action != null)
                        action();
                    else
                        pos.OnBarcodeInput(input);
                }
                catch (Exception e)
                {
                    Console.WriteLine(String.Format("Command error: {0}\nKnown commands: {1}",
                        e.Message, String.Join(", ", actions.Keys)));
                }
            }
        }
    }
}