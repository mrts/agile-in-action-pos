﻿using AgileInAction.Pos.Core.Presentation;
using System;

namespace AgileInAction.Pos.Ui.ConsoleUi
{
    public class ConsoleDisplay : IDisplay
    {
        public void ShowError(string message)
        {
            Console.WriteLine("Error: " + message);
        }

        public void ShowTotal(decimal amount)
        {
            Console.WriteLine("Total: " + amount.ToString());
        }

        public void ShowSaleIsCompleted()
        {
            Console.WriteLine("Sale is completed. You can start a new sale now.");
        }
    }
}