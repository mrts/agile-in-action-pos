using AgileInAction.Pos.Core.DataAccess;
using AgileInAction.Pos.Core.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace AgileInAction.Pos.Data.XmlStore
{
    public class XmlFileRepository<T> : GenericRepository<T> where T : IEntity
    {
        private readonly string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
            "AgileInAction.Pos.Xml" + typeof(T).Name + "Repository.xml");

        private readonly XmlSerializer xmlSerializer;

        public XmlFileRepository()
        {
            xmlSerializer = new XmlSerializer(entities.GetType());
            LoadFromFile();
        }

        public override void SaveOrUpdate(T entity)
        {
            base.SaveOrUpdate(entity);
            SaveToFile();
        }

        public override void Delete(T entity)
        {
            base.Delete(entity);
            SaveToFile();
        }

        private void LoadFromFile()
        {
            if (!File.Exists(filePath))
                return;

            using (var reader = new StreamReader(filePath))
            {
                entities = (List<T>)xmlSerializer.Deserialize(reader);
            }
        }

        private void SaveToFile()
        {
            using (var writer = new StreamWriter(filePath))
            {
                xmlSerializer.Serialize(writer, entities);
            }
        }
    }
}