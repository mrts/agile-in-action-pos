﻿using AgileInAction.Pos.Core;
using AgileInAction.Pos.Core.DataAccess;
using AgileInAction.Pos.Core.Model;
using AgileInAction.Pos.Core.Presentation;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace AgileInAction.Pos.Tests
{
    [TestFixture]
    public class SellItemsTests
    {
        private GenericRepository<Product> mockProductRepository;
        private GenericRepository<Sale> mockSaleRepository;
        private PointOfSale pos;
        private Mock<IDisplay> mockDisplay;

        [SetUp]
        public void SetUp()
        {
            mockSaleRepository = new GenericRepository<Sale>();
            mockProductRepository = new GenericRepository<Product>();
            mockDisplay = new Mock<IDisplay>();
            pos = new PointOfSale(mockSaleRepository, mockProductRepository, mockDisplay.Object);
        }

        [Test]
        public void StartSale()
        {
            // arrange - nothing to do, only assert that sale repo is empty
            Assert.That(mockSaleRepository.Query().Count(), Is.EqualTo(0));

            // act
            pos.OnStartSale();

            // assert
            Assert.That(mockSaleRepository.Query().Count(), Is.EqualTo(1));
            Assert.That(mockSaleRepository.Query().FirstOrDefault().IsCompleted, Is.False);
        }

        [Test]
        public void SellSingleItem()
        {
            // arrange
            var product = new Product() { Id = Guid.NewGuid().ToString(), Barcode = "123" };
            mockProductRepository.SaveOrUpdate(product);

            // act
            pos.OnStartSale();
            pos.OnBarcodeInput("123");

            // assert
            Assert.That(mockSaleRepository.Query().FirstOrDefault().SaleItems[0].Id, Is.EqualTo(product.Id));
        }

        [Test]
        public void SellSingleItem_WhenProductDoesNotExist_ThenDisplaysError()
        {
            // arrange
            Assert.That(mockProductRepository.Query().Count(), Is.EqualTo(0));

            // act
            pos.OnStartSale();
            pos.OnBarcodeInput("123");

            // assert
            mockDisplay.Verify(display => display.ShowError("Product with barcode '123' not found"), Times.Exactly(1));
        }

        [Test]
        public void ShowTotal()
        {
            // arrange
            var product1 = new Product() { Id = Guid.NewGuid().ToString(), Barcode = "123", Price = 1.03m };
            mockProductRepository.SaveOrUpdate(product1);
            var product2 = new Product() { Id = Guid.NewGuid().ToString(), Barcode = "124", Price = 1.04m };
            mockProductRepository.SaveOrUpdate(product2);

            // act
            pos.OnStartSale();
            pos.OnBarcodeInput("123");
            pos.OnBarcodeInput("123");
            pos.OnBarcodeInput("124");
            pos.OnShowTotal();

            // assert
            mockDisplay.Verify(display => display.ShowTotal(product1.Price * 2 + product2.Price), Times.Exactly(1));
        }

        [Test]
        public void CompleteSale()
        {
            var product = new Product() { Id = Guid.NewGuid().ToString(), Barcode = "123" };
            mockProductRepository.SaveOrUpdate(product);

            // act
            pos.OnStartSale();
            pos.OnBarcodeInput("123");
            pos.OnCompleteSale();

            // assert
            Assert.That(mockSaleRepository.Query().FirstOrDefault().IsCompleted, Is.True);
            mockDisplay.Verify(display => display.ShowTotal(product.Price), Times.Exactly(1));
            mockDisplay.Verify(display => display.ShowSaleIsCompleted(), Times.Exactly(1));
        }
    }
}