﻿namespace AgileInAction.Pos.Core.Model
{
    public class Product : IEntity
    {
        public string Barcode { get; set; }

        public decimal Price { get; set; }
    }
}