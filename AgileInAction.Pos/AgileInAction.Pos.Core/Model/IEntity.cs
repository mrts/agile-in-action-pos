﻿namespace AgileInAction.Pos.Core.Model
{
    public abstract class IEntity
    {
        public string Id { get; set; }
    }
}