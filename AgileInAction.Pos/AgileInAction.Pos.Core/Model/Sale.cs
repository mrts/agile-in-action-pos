using System.Collections.Generic;

namespace AgileInAction.Pos.Core.Model
{
    public class Sale : IEntity
    {
        public List<Product> SaleItems { get; set; }

        public bool IsCompleted { get; set; }
    }
}