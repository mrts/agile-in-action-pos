﻿namespace AgileInAction.Pos.Core.Presentation
{
    public interface IDisplay
    {
        void ShowError(string message);

        void ShowTotal(decimal amount);

        void ShowSaleIsCompleted();
    }
}