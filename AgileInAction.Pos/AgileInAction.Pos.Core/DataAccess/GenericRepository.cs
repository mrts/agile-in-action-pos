﻿using AgileInAction.Pos.Core.Model;
using System.Collections.Generic;
using System.Linq;

namespace AgileInAction.Pos.Core.DataAccess
{
    public class GenericRepository<T> : IRepository<T> where T : IEntity
    {
        protected List<T> entities = new List<T>();

        public T Get(string id)
        {
            return entities.FirstOrDefault(entity => entity.Id == id);
        }

        public virtual void SaveOrUpdate(T entity)
        {
            if (entities.Contains(entity))
                entities.Remove(entity);
            entities.Add(entity);
        }

        public virtual void Delete(T entity)
        {
            entities.RemoveAll(e => e.Id == entity.Id);
        }

        public IQueryable<T> Query()
        {
            return entities.AsQueryable();
        }
    }
}