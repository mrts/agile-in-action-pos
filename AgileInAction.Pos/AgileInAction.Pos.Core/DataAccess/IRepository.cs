using AgileInAction.Pos.Core.Model;
using System.Linq;

namespace AgileInAction.Pos.Core.DataAccess
{
    public interface IRepository<T> where T : IEntity
    {
        T Get(string id);

        void SaveOrUpdate(T entity);

        void Delete(T entity);

        IQueryable<T> Query();
    }
}