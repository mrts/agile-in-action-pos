﻿using AgileInAction.Pos.Core.DataAccess;
using AgileInAction.Pos.Core.Model;
using AgileInAction.Pos.Core.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AgileInAction.Pos.Core
{
    public class PointOfSale
    {
        private readonly IRepository<Sale> saleRepository;
        private readonly IRepository<Product> productRepository;
        private readonly IDisplay display;
        private Sale sale;

        public PointOfSale(IRepository<Sale> saleRepository, IRepository<Product> productRepository, IDisplay display)
        {
            this.saleRepository = saleRepository;
            this.productRepository = productRepository;
            this.display = display;
        }

        public void OnStartSale()
        {
            sale = new Sale { Id = Guid.NewGuid().ToString(), SaleItems = new List<Product>(), IsCompleted = false };
            saleRepository.SaveOrUpdate(sale);
        }

        public void OnBarcodeInput(string barcode)
        {
            var product = productRepository.Query().FirstOrDefault(p => p.Barcode == barcode);
            if (product == null)
            {
                display.ShowError(String.Format("Product with barcode '{0}' not found", barcode));
                return;
            }

            sale.SaleItems.Add(product);
            saleRepository.SaveOrUpdate(sale);
        }

        public void OnShowTotal()
        {
            var total = sale.SaleItems.Sum(p => p.Price);
            display.ShowTotal(total);
        }

        public void OnCompleteSale()
        {
            sale.IsCompleted = true;
            saleRepository.SaveOrUpdate(sale);

            OnShowTotal();
            display.ShowSaleIsCompleted();

            sale = null;
        }
    }
}