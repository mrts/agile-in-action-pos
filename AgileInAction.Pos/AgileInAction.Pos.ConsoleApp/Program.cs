﻿using AgileInAction.Pos.Core;
using AgileInAction.Pos.Core.Model;
using AgileInAction.Pos.Data.XmlStore;
using AgileInAction.Pos.Ui.ConsoleUi;
using System;

namespace AgileInAction.Pos.ConsoleApp
{
    internal class MainClass
    {
        public static void Main(string[] args)
        {
            try
            {
                var saleRepository = new XmlFileRepository<Sale>();
                var productRepository = new XmlFileRepository<Product>();
                var display = new ConsoleDisplay();

                var pos = new PointOfSale(saleRepository, productRepository, display);

                var runner = new ConsoleUiRunner(pos);
                runner.RunReadEvalPrintLoop();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fatal error: " + ex.Message);
                Console.ReadLine();
            }
        }
    }
}