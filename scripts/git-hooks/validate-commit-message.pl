#!perl
use strict;
use warnings;

my $fileName = $ARGV[0];
my $FILE;
open FILE, $fileName;

my $commitMsg = <FILE>;
my $jiraKeys = 'AIAPOS-\d{2,6}';
my $regex = "^($jiraKeys(, $jiraKeys)?: \\w{3,}|Merge )";

# Simple #123-style refs:
# my $regex = '^(#\d{3,6}(, #\d{3,6})?: \w{3,}|Merge )';

if ($commitMsg !~ /$regex/) {
    $commitMsg =~ s/\s+$//;
    print "[COMMIT HOOK] Commit message '$commitMsg' is not formatted correctly.\n";
    print "Please correct the message and then commit again.\n";
    print "Commit message should be formatted according to the following template:\n\n";
    print "  <JIRA task ID>[, <JIRA task ID>]: description\n";
    exit 1;
}

exit 0;
