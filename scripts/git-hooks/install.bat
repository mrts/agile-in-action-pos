@echo off
setlocal enableextensions
cd /d "%~dp0"

echo on

copy *commit-m* ..\..\.git\hooks

@echo.
@echo If 2 files were copied without errors then
@echo git hooks have been successfully installed
@echo.

@pause
